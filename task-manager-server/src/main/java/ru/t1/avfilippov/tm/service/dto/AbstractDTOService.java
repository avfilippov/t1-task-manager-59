package ru.t1.avfilippov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.repository.dto.IDTORepository;
import ru.t1.avfilippov.tm.api.service.dto.IService;
import ru.t1.avfilippov.tm.dto.model.AbstractModelDTO;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    private IDTORepository<M> repository;


    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        return repository.add(model);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public @Nullable List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        if (model == null) return;
        repository.remove(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        if (model == null) return;
        repository.update(model);
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        @NotNull M model = findOneById(id);
        remove(model);
    }

}
