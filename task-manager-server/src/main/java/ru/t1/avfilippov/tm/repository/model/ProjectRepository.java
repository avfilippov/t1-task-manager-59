package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.model.IProjectRepository;
import ru.t1.avfilippov.tm.model.Project;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    @NotNull
    protected Class<Project> getClazz() {
        return Project.class;
    }

    @Override
    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @Override
    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}
