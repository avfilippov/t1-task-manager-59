package ru.t1.avfilippov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.repository.model.IProjectRepository;
import ru.t1.avfilippov.tm.api.service.model.IProjectService;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.*;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.repository.model.ProjectRepository;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractUserOwnedService<Project, ProjectRepository>
        implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Override
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.update(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return repository.add(userId, project);
    }

    @Override
    @NotNull
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return repository.add(userId, project);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        if (description != null) project.setDescription(description);
        repository.update(userId, project);
    }

}
