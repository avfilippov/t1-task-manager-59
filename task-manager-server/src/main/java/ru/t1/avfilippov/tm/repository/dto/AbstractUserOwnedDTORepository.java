package ru.t1.avfilippov.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.avfilippov.tm.dto.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModel>
        extends AbstractDTORepository<M>
        implements IUserOwnedDTORepository<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getClazz());
        @NotNull final Root<M> root = criteriaQuery.from(getClazz());
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        entityManager.merge(model);
    }

}
