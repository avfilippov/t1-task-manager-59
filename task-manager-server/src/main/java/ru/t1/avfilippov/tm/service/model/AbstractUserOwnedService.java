package ru.t1.avfilippov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.avfilippov.tm.api.service.model.IUserOwnedService;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.model.AbstractUserOwnedModel;
import ru.t1.avfilippov.tm.model.User;

import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(model);
    }

    @NotNull
    @Autowired
    protected IUserOwnedRepository<M> repository;

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @Override
    @Nullable
    public List<M> findAll(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    @Nullable
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.remove(userId, model);
    }

    @Override
    @Transactional
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.update(userId, model);
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = findOneById(userId, id);
        if (model != null) remove(userId, model);
    }

}
