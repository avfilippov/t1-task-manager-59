package ru.t1.avfilippov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.repository.model.ITaskRepository;
import ru.t1.avfilippov.tm.api.service.model.IProjectService;
import ru.t1.avfilippov.tm.api.service.model.ITaskService;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.exception.field.*;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.repository.model.TaskRepository;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskService extends AbstractUserOwnedService<Task, TaskRepository>
        implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusNotFoundException();
        task.setStatus(status);
        repository.update(userId, task);
    }

    @Override
    @NotNull
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        return repository.add(userId, task);
    }

    @Override
    @NotNull
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return repository.add(userId, task);
    }

    @Override
    @Nullable
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null) task.setDescription(description);
        repository.update(userId, task);
    }

    @Override
    @Transactional
    public void updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectService.findOneById(projectId));
        repository.update(userId, task);
    }

}
