package ru.t1.avfilippov.tm.api.repository.model;

import ru.t1.avfilippov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
