package ru.t1.avfilippov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

import javax.persistence.EntityManager;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login,
                @NotNull String password,
                @Nullable String email);

    @NotNull
    User create(@NotNull String login,
                @NotNull String password,
                @Nullable Role role);

    @NotNull
    User create(@NotNull String login,
                @NotNull String password,
                @Nullable String email,
                @NotNull String lastname,
                @NotNull String firstName,
                @Nullable String middleName);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    Boolean isEmailExists(@NotNull String email);

    Boolean isLoginExists(@NotNull String login);

    @NotNull EntityManager getEntityManager();

}
