package ru.t1.avfilippov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class SessionSchemeTest extends AbstractSchemeTest {

    @Test
    public void Test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("session");
    }

}
