package ru.t1.avfilippov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.event.ConsoleEvent;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void handler(ConsoleEvent consoleEvent) throws Exception;

}
