package ru.t1.avfilippov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ServerAboutRequest extends AbstractUserRequest {

    public ServerAboutRequest(@Nullable final String token) {
        super(token);
    }

}
