package ru.t1.avfilippov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ProjectUpdateByIdRequest;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "update project by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setProjectId(id);
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.updateProjectById(request);
    }

}
