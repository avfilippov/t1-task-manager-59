package ru.t1.avfilippov.tm.listener;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.api.service.ITokenService;
import ru.t1.avfilippov.tm.enumerated.Role;

@Getter
@Setter
@Component
@NoArgsConstructor
public abstract class AbstractListener implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
