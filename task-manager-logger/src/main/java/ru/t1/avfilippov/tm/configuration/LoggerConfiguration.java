package ru.t1.avfilippov.tm.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.avfilippov.tm")
public class LoggerConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        connectionFactory.setTrustAllPackages(true);
        return connectionFactory;
    }

}
